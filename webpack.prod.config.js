let path=require('path');
let webpack=require('webpack');
let htmlWebpackPlugin=require('html-webpack-plugin');
let VueLoaderPlugin=require('vue-loader/lib/plugin');
module.exports={
	//这里专门用于开发调试
	// devtool:'inline-eval-cheap-source-map',//在浏览器端，多出一个webpack文件夹，可以进行源码调试。但是在文件夹里看不到map文件
	//devtool:'inline-source-map',//在浏览器端，多出一个webpack文件夹，可以进行源码调试。但是在文件夹里看不到map文件
	// devtool:'source-map',//在浏览器端，多出一个webpack文件夹，可以进行源码调试。在文件夹里也能看到map文件
	// devtool:'cheap-source-map',//可生成map文件，在浏览器端也会多出一个webpack文件夹，但是文件依然是压缩过的，没法进行断点调试
	//devtool:'cheap-module-source-map',//可生成map文件，在浏览器端也会多出一个webpack文件夹，但文件夹里只包含部分文件，并不是所有，尤其是自己编写的js没有被引入
	
	//入口文件
	entry:{
		index:'./src/js/main.js',
		users:'./src/js/users.js'
	},
	//编译后的文件及路径
	output:{
		filename:"javascripts/[name]-bundle.js",
		//这里直接使用文件路径拼装
		path:__dirname+'/public',
		//publicPath在生成的html模板中非常有用
		// publicPath:'http://localhost:3000'
	},
	module:{
		rules:[
			//css加载器
			{ test:/\.css$/,exclude: /(node_modules|bower_components)/,loader:'style-loader!css-loader' },
			//由于bootstrap依赖jquery，这里全局引用jquery
			//expose-loader 用来把模块暴露到全局变量。这个功对调试或者支持依赖其他全局库的库时很有用。
			{test:require.resolve("jquery"), loader:"expose-loader?$!expose-loader?jQuery"},
			{test:/\.woff(\?v=\d+\.\d+\.\d+)?$/, loader:"url-loader?limit=10000&mimetype=application/font-woff"},
			{test:/\.woff2(\?v=\d+\.\d+\.\d+)?$/, loader:"url-loader?limit=10000&mimetype=application/font-woff2"},
			{test:/\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader:"url-loader?limit=10000&mimetype=application/octet-stream"},
			{test:/\.eot(\?v=\d+\.\d+\.\d+)?$/, loader:"file-loader"},
			{test:/\.svg(\?v=\d+\.\d+\.\d+)?$/, loader:"url-loader?limit=10000&mimetype=application/svg+xml"},
			//handlebars
			{ test: /\.hbs$/, loader: "handlebars-loader" },
			{ test: /\.vue$/, loader:'vue-loader'},
			{ test: /\.(js|jsx)$/,exclude: /(node_modules|bower_components)/,loader: 'babel-loader',options: {'presets': ['latest']} }
		]
	},
	resolve:{
		alias:{
			'vue':'vue/dist/vue.js'
		}
	},
	plugins:[
	// 	new htmlWebpackPlugin({
	// 	    title: 'Custom template using Handlebars',
	// 	    template: './views/layout.hbs',
	// 	    inject: 'body'
	// 	})
	new webpack.ProvidePlugin({
		// jQUery:'jquery',
		// $:'jquery',
		Popper:['popper.js']
	}),
	new VueLoaderPlugin()
	]
}