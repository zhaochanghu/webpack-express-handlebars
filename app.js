let express=require('express');
let path = require('path');
let favicon=require('serve-favicon');
let cookieParser = require('cookie-parser');
let bodyParser=require('body-parser');
let logger=require('morgan');

//在express中加载webpack模块
let webpack = require('webpack');
//引入webpack的配置文件
let webpackConfig = process.env.NODE_ENV === 'production' ? require('./webpack.prod.conf.js'):require('./webpack.dev.conf.js');
// let webpackConfig = require('./webpack.dev.conf.js');
// console.log(webpackConfig);
//启动webpack的方法  webpack(配置文件对象，回调)
let compiler = webpack(webpackConfig,(err,stats)=>{
	//我们可以在stats中看到webpack打包的过程与命令行执行的结果是一样的
	console.log(stats.toString({
		colors:true
	}));
	//通过compiler对象可以开启watch模式来监听原文件的变化，
	//如果原文件发生改变就会触发webpack的重新打包
	//回调函数内部与打包函数是一样的
	compiler.watch({
		aggregateTimeout:300,//防止重复按键，300毫秒算按键一次
		poll:1000,//检测修改的时间（ms）
		ignored:/node_modules/,//不监测
	},(err,stats)=>{

	})
});

let index = require('./routes/index');
let users = require('./routes/users');

let app=express();

app.set('views',path.join(__dirname,'views'));
app.set('view engine','hbs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname,'public')));

app.use('/',index);
app.use('/users',users);

app.use((req,res,next)=>{
	let err= new Error('Not Found');
	err.status = 404;
	next(err);
});

app.use((req,res,next)=>{
	res.locals.message=err.message;
	res.locals.error=req.app.get('env') ==='development'?err:{};

	res.status(err.status||500);
	res.render('error');
})

module.exports =app;
