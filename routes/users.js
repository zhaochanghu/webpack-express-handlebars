var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('users',{username:'Lucy',school:'MIT',layout:'layout2'})
});

module.exports = router;